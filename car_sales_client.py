import sys
from datetime import date
import grpc
from google.protobuf import empty_pb2

from car_sales.gen_py import car_sales_pb2_grpc as cs_service
from car_sales.gen_py import car_sales_pb2 as cs_messages


def run():
    channel = grpc.insecure_channel('localhost:50051')

    try:
        grpc.channel_ready_future(channel).result(timeout=10)
    except grpc.FutureTimeoutError:
        sys.exit('Error connecting to server')
    else:
        stub = cs_service.Car_salesStub(channel)
        response = stub.getAllPosts(empty_pb2.Empty())
        print(response)
        # print("*"*20)
        # response = stub.getAllCarProfile(empty_pb2.Empty())
        #
        # response = stub.getAllUsers(empty_pb2.Empty())
        # print(response)
        # response = stub.addPost(cs_messages.CarPost(
        #     user=cs_messages.User(name="ahmadreza"),
        #     car_profile=cs_messages.CarProfile(name="prisde jdsdadid",details="hsdichi",color="whsdite"),
        # ))

        response = stub.addOffer(cs_messages.CarOffer(
            user_name="ahmadreza",
            car_post_id=46,
            suggestion=1000,
        ))


        print(response)


if __name__ == '__main__':
    run()
