python -m grpc_tools.protoc --proto_path=./car_sales/protos/ --python_out=./car_sales/gen_py --grpc_python_out=./car_sales/gen_py ./car_sales/protos/*.proto;

sed -i "4s/.*/from . import car_sales_pb2 as car__sales__pb2/" ./car_sales/gen_py/car_sales_pb2_grpc.py;
