from contextlib import contextmanager
from django.core.management.base import BaseCommand, CommandError
from concurrent import futures
from car_sales.services.car_sales_service import CarSalesAPI

import car_sales.gen_py.car_sales_pb2_grpc as cs_service
import grpc
import time

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


@contextmanager
def serve_forever():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    cs_service.add_Car_salesServicer_to_server(CarSalesAPI(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    yield
    server.stop(0)


class Command(BaseCommand):
    help = 'api server'

    def handle(self, *args, **options):
        with serve_forever():
            self.stdout.write(self.style.SUCCESS(
                'Successfully started grpc server '))
            try:
                while True:
                    time.sleep(_ONE_DAY_IN_SECONDS)
            except KeyboardInterrupt:
                pass