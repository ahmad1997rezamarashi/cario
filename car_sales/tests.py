import json
from datetime import date
from random import randint
from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate

import car_sales.views as views
from .models import CarPost, CarProfile, CarOffer, User
from .serializers import CarProfileSerializer, CarOfferSerializer, CarPostSerializer


class TestCarAPI(TestCase):
    def setUp(self):
        self.get_all_posts_url = "/all_posts/"
        self.add_car_post_url = '/add_post/'
        self.add_car_offer_url = '/add_offer/'
        self.create_auth_url = '/create_auth/'

        self.factory = APIRequestFactory()
        self.user1 = User.objects.create_user(username="user1", password="pass", email="user1@Cario.marash")
        self.user2 = User.objects.create_user(username="user2", password="pass", email="user2@Cario.marash")
        self.car_profiles = []
        self.car_posts = []

        for i in range(3):
            car_profile = CarProfile.objects.create(name=f"car_name{i}", details=f"car_details{i}",
                                                    color=f"car_color{i}")
            car_post = CarPost.objects.create(car_profile=car_profile, created=date.today(), user=self.user1)
            self.car_profiles.append(car_profile)
            self.car_posts.append(car_post)

        for i in range(3):
            CarOffer.objects.create(
                user=self.user1,
                car_post=self.car_posts[i],
                created=date.today(),
                suggestion=randint(1000, 2000)
            )

    def test_get_all_posts(self):
        request = self.factory.get(self.get_all_posts_url)
        response = views.get_all_post(request)
        self.assertIs(response.status_code, 200)
        car_post_list = [CarPostSerializer(car_post).data for car_post in CarPost.objects.all()]
        self.assertEqual(car_post_list, response.data)

    def test_add_car_offer(self):
        request = self.factory.post(self.add_car_offer_url, {"suggestion": 1997, "post_id": self.car_posts[0].id})
        force_authenticate(request, user=self.user1)
        response = views.add_offer(request)
        self.assertIs(response.status_code, 200)
        car_offer = CarOffer.objects.get(car_post=self.car_posts[0], suggestion=1997, created=date.today())
        self.assertEqual(CarOfferSerializer(car_offer).data, response.data)

    def test_add_car_post(self):
        car_profile_obj = {"name": "name", "color": "color", "details": "details"}

        request = self.factory.post(self.add_car_post_url, car_profile_obj)

        force_authenticate(request, user=self.user1)

        response = views.add_post(request)

        new_car_post_id = response.data.get("id")
        new_car_post = CarPost.objects.get(id=new_car_post_id)
        new_car_profile = new_car_post.car_profile

        self.assertIs(response.status_code, 200)
        self.assertEqual(CarProfileSerializer(new_car_profile).data, car_profile_obj)
