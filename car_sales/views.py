from datetime import date

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import APIException
from rest_framework.permissions import IsAuthenticated  # <-- Here
from rest_framework.response import Response

from .forms import UserRegisterForm, UserForm
from .models import CarPost, CarOffer, CarProfile, User
from .serializers import CarOfferSerializer, CarPostSerializer, UserSerializer


def index(request):
    car_posts_list = CarPost.objects.all()

    posts_list = []
    for car_post_item in car_posts_list:
        offers_list = CarOffer.objects.filter(
            car_post=car_post_item).order_by("-suggestion")

        posts_list.append({
            'id': car_post_item.id,
            'name': car_post_item.car_profile.name,
            'details': car_post_item.car_profile.details,
            'owner': car_post_item.user,
            'offers_list': offers_list})
    return render(request, 'car_sales/index.html', {'posts_list': posts_list})


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                messages.success(request, "You are login")
                return redirect("home")
            else:
                messages.warning(request, "Your account was inactive.")
                return render(request, 'car_sales/login.html', {})
        else:
            messages.warning(request, "Wrong username or password")
            return render(request, 'car_sales/login.html', {})
    else:
        return render(request, 'car_sales/login.html', {})


def register(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        profile_form = UserForm(request.POST)
        if form.is_valid() and profile_form.is_valid():
            username = form.cleaned_data.get("username")
            messages.success(
                request, 'account created for {}'.format(username))

            user = form.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile_form.save()

            return redirect("home")
    else:
        form = UserRegisterForm()
        profile_form = UserForm()

    return render(request, 'car_sales/register.html', {'form': form, 'profile_form': profile_form})


@api_view(['GET'])
def get_all_post(request):
    car_posts_list = CarPost.objects.all()
    posts_list = [CarPostSerializer(car_post_item).data for car_post_item in car_posts_list]
    return Response(posts_list)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_post(request):
    try:
        user = request.user
        car_name = request.data.get("name")
        car_color = request.data.get("color")
        car_details = request.data.get("details")
        car_profile = CarProfile(name=car_name, details=car_details, color=car_color)
        car_profile.save()
        car_post = CarPost(car_profile=car_profile, user=user)
        car_post.save()

        return Response(CarPostSerializer(car_post).data)
    except Exception as err:
        raise APIException(err)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_offer(request):
    try:
        user = request.user
        car_post_id = request.data.get("post_id")
        offer_suggestion = request.data.get("suggestion")
        car_post = CarPost.objects.get(id=car_post_id)
        car_offer = CarOffer(user=user, car_post=car_post,
                             suggestion=offer_suggestion)
        car_offer.save()

        return Response(CarOfferSerializer(car_offer).data)
    except Exception as err:
        raise APIException(err)


@api_view(['POST'])
def create_auth(request):
    try:
        username = request.data.get("username")
        password = request.data.get("password")
        email = request.data.get("email")

        user = User.objects.create_user(
            username=username,
            password=password,
            email=email)
        return Response(UserSerializer(user).data, status=status.HTTP_201_CREATED)

    except Exception as err:
        raise APIException(err)
