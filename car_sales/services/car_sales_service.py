from django.db import transaction
from google.protobuf import timestamp_pb2, empty_pb2

import car_sales.gen_py.car_sales_pb2 as cs_messages
import car_sales.gen_py.car_sales_pb2_grpc as car_sales_service
from car_sales.models import CarPost, CarProfile, CarOffer, User

from datetime import datetime, date


def convert_data_to_timstamp(data_obj):
    date_time_obj = datetime.combine(data_obj, datetime.min.time())
    timestamp = timestamp_pb2.Timestamp()
    timestamp.FromDatetime(date_time_obj)
    return timestamp


class CarSalesAPI(car_sales_service.Car_salesServicer):

    def getAllPosts(self, request, context):

        post_list = CarPost.objects.all()
        response = cs_messages.CarPostsList()

        for car_post in post_list:
            car_profile = cs_messages.CarProfile(
                name=car_post.car_profile.name,
                details=car_post.car_profile.details,
                color=car_post.car_profile.color,
            )
            user = cs_messages.User(
                name=car_post.user.username,
                email=car_post.user.email
            )

            car_post_item = cs_messages.CarPost(
                id=car_post.id,
                user=user,
                car_profile=car_profile,
                created=convert_data_to_timstamp(car_post.created)
            )

            response.car_post_list.append(car_post_item)

        return response

    def getAllCarProfile(self, request, context):

        car_profile_list = CarProfile.objects.all()
        response = cs_messages.CarProfilesList()

        for car_profile in car_profile_list:
            car_profile_item = cs_messages.CarProfile(
                name=car_profile.name,
                details=car_profile.details,
                color=car_profile.color
            )
            response.car_profile_list.append(car_profile_item)

        return response

    def getAllUsers(self, request, context):

        user_list = User.objects.all()
        response = cs_messages.UsersList()

        for user in user_list:
            user_item = cs_messages.User(
                name=user.username,
                email=user.email
            )
            response.user_list.append(user_item)

        return response

    @transaction.atomic
    def addPost(self, request, context):

        car_profile = CarProfile.objects.create(
            name=request.car_profile.name,
            details=request.car_profile.details,
            color=request.car_profile.color,
        )
        user = User.objects.get(username=request.user.name)

        CarPost.objects.create(
            car_profile=car_profile,
            user=user,
        )

        return empty_pb2.Empty()

    def addOffer(self, request, context):
        user = User.objects.get(username=request.user_name)
        car_post = CarPost.objects.get(id=request.car_post_id)
        CarOffer.objects.create(
            user=user,
            car_post=car_post,
            suggestion=request.suggestion,
            created=date.today()
        )
        return empty_pb2.Empty()
