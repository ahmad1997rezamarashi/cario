from django.apps import AppConfig


class CarSales(AppConfig):
    name = 'car_sales'
