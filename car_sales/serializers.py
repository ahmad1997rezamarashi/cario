from .models import CarProfile, CarPost, CarOffer
from rest_framework import serializers
from django.contrib.auth.models import User


class CarProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarProfile
        fields = ('name', 'details','color')


class CarPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarPost
        fields = ('id','user', 'car_profile', 'created',)


class CarOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarOffer
        fields = ('suggestion', 'created',)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
