# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: car_sales.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import empty_pb2 as google_dot_protobuf_dot_empty__pb2
from google.protobuf import timestamp_pb2 as google_dot_protobuf_dot_timestamp__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='car_sales.proto',
  package='car_sales',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=b'\n\x0f\x63\x61r_sales.proto\x12\tcar_sales\x1a\x1bgoogle/protobuf/empty.proto\x1a\x1fgoogle/protobuf/timestamp.proto\"#\n\x04User\x12\x0c\n\x04name\x18\x02 \x01(\t\x12\r\n\x05\x65mail\x18\x03 \x01(\t\"\x8d\x01\n\x07\x43\x61rPost\x12\n\n\x02id\x18\x01 \x01(\x03\x12*\n\x0b\x63\x61r_profile\x18\x02 \x01(\x0b\x32\x15.car_sales.CarProfile\x12\x1d\n\x04user\x18\x03 \x01(\x0b\x32\x0f.car_sales.User\x12+\n\x07\x63reated\x18\x04 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\":\n\nCarProfile\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x0f\n\x07\x64\x65tails\x18\x02 \x01(\t\x12\r\n\x05\x63olor\x18\x03 \x01(\t\"s\n\x08\x43\x61rOffer\x12\x11\n\tuser_name\x18\x02 \x01(\t\x12\x13\n\x0b\x63\x61r_post_id\x18\x01 \x01(\x03\x12\x12\n\nsuggestion\x18\x03 \x01(\x03\x12+\n\x07\x63reated\x18\x04 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\"9\n\x0c\x43\x61rPostsList\x12)\n\rcar_post_list\x18\x01 \x03(\x0b\x32\x12.car_sales.CarPost\"B\n\x0f\x43\x61rProfilesList\x12/\n\x10\x63\x61r_profile_list\x18\x01 \x03(\x0b\x32\x15.car_sales.CarProfile\"/\n\tUsersList\x12\"\n\tuser_list\x18\x01 \x03(\x0b\x32\x0f.car_sales.User2\xc0\x02\n\tCar_sales\x12>\n\x0bgetAllPosts\x12\x16.google.protobuf.Empty\x1a\x17.car_sales.CarPostsList\x12;\n\x0bgetAllUsers\x12\x16.google.protobuf.Empty\x1a\x14.car_sales.UsersList\x12\x46\n\x10getAllCarProfile\x12\x16.google.protobuf.Empty\x1a\x1a.car_sales.CarProfilesList\x12\x35\n\x07\x61\x64\x64Post\x12\x12.car_sales.CarPost\x1a\x16.google.protobuf.Empty\x12\x37\n\x08\x61\x64\x64Offer\x12\x13.car_sales.CarOffer\x1a\x16.google.protobuf.Emptyb\x06proto3'
  ,
  dependencies=[google_dot_protobuf_dot_empty__pb2.DESCRIPTOR,google_dot_protobuf_dot_timestamp__pb2.DESCRIPTOR,])




_USER = _descriptor.Descriptor(
  name='User',
  full_name='car_sales.User',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='car_sales.User.name', index=0,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='email', full_name='car_sales.User.email', index=1,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=92,
  serialized_end=127,
)


_CARPOST = _descriptor.Descriptor(
  name='CarPost',
  full_name='car_sales.CarPost',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='car_sales.CarPost.id', index=0,
      number=1, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='car_profile', full_name='car_sales.CarPost.car_profile', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='user', full_name='car_sales.CarPost.user', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='created', full_name='car_sales.CarPost.created', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=130,
  serialized_end=271,
)


_CARPROFILE = _descriptor.Descriptor(
  name='CarProfile',
  full_name='car_sales.CarProfile',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='car_sales.CarProfile.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='details', full_name='car_sales.CarProfile.details', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='color', full_name='car_sales.CarProfile.color', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=273,
  serialized_end=331,
)


_CAROFFER = _descriptor.Descriptor(
  name='CarOffer',
  full_name='car_sales.CarOffer',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='user_name', full_name='car_sales.CarOffer.user_name', index=0,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='car_post_id', full_name='car_sales.CarOffer.car_post_id', index=1,
      number=1, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='suggestion', full_name='car_sales.CarOffer.suggestion', index=2,
      number=3, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='created', full_name='car_sales.CarOffer.created', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=333,
  serialized_end=448,
)


_CARPOSTSLIST = _descriptor.Descriptor(
  name='CarPostsList',
  full_name='car_sales.CarPostsList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='car_post_list', full_name='car_sales.CarPostsList.car_post_list', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=450,
  serialized_end=507,
)


_CARPROFILESLIST = _descriptor.Descriptor(
  name='CarProfilesList',
  full_name='car_sales.CarProfilesList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='car_profile_list', full_name='car_sales.CarProfilesList.car_profile_list', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=509,
  serialized_end=575,
)


_USERSLIST = _descriptor.Descriptor(
  name='UsersList',
  full_name='car_sales.UsersList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='user_list', full_name='car_sales.UsersList.user_list', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=577,
  serialized_end=624,
)

_CARPOST.fields_by_name['car_profile'].message_type = _CARPROFILE
_CARPOST.fields_by_name['user'].message_type = _USER
_CARPOST.fields_by_name['created'].message_type = google_dot_protobuf_dot_timestamp__pb2._TIMESTAMP
_CAROFFER.fields_by_name['created'].message_type = google_dot_protobuf_dot_timestamp__pb2._TIMESTAMP
_CARPOSTSLIST.fields_by_name['car_post_list'].message_type = _CARPOST
_CARPROFILESLIST.fields_by_name['car_profile_list'].message_type = _CARPROFILE
_USERSLIST.fields_by_name['user_list'].message_type = _USER
DESCRIPTOR.message_types_by_name['User'] = _USER
DESCRIPTOR.message_types_by_name['CarPost'] = _CARPOST
DESCRIPTOR.message_types_by_name['CarProfile'] = _CARPROFILE
DESCRIPTOR.message_types_by_name['CarOffer'] = _CAROFFER
DESCRIPTOR.message_types_by_name['CarPostsList'] = _CARPOSTSLIST
DESCRIPTOR.message_types_by_name['CarProfilesList'] = _CARPROFILESLIST
DESCRIPTOR.message_types_by_name['UsersList'] = _USERSLIST
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

User = _reflection.GeneratedProtocolMessageType('User', (_message.Message,), {
  'DESCRIPTOR' : _USER,
  '__module__' : 'car_sales_pb2'
  # @@protoc_insertion_point(class_scope:car_sales.User)
  })
_sym_db.RegisterMessage(User)

CarPost = _reflection.GeneratedProtocolMessageType('CarPost', (_message.Message,), {
  'DESCRIPTOR' : _CARPOST,
  '__module__' : 'car_sales_pb2'
  # @@protoc_insertion_point(class_scope:car_sales.CarPost)
  })
_sym_db.RegisterMessage(CarPost)

CarProfile = _reflection.GeneratedProtocolMessageType('CarProfile', (_message.Message,), {
  'DESCRIPTOR' : _CARPROFILE,
  '__module__' : 'car_sales_pb2'
  # @@protoc_insertion_point(class_scope:car_sales.CarProfile)
  })
_sym_db.RegisterMessage(CarProfile)

CarOffer = _reflection.GeneratedProtocolMessageType('CarOffer', (_message.Message,), {
  'DESCRIPTOR' : _CAROFFER,
  '__module__' : 'car_sales_pb2'
  # @@protoc_insertion_point(class_scope:car_sales.CarOffer)
  })
_sym_db.RegisterMessage(CarOffer)

CarPostsList = _reflection.GeneratedProtocolMessageType('CarPostsList', (_message.Message,), {
  'DESCRIPTOR' : _CARPOSTSLIST,
  '__module__' : 'car_sales_pb2'
  # @@protoc_insertion_point(class_scope:car_sales.CarPostsList)
  })
_sym_db.RegisterMessage(CarPostsList)

CarProfilesList = _reflection.GeneratedProtocolMessageType('CarProfilesList', (_message.Message,), {
  'DESCRIPTOR' : _CARPROFILESLIST,
  '__module__' : 'car_sales_pb2'
  # @@protoc_insertion_point(class_scope:car_sales.CarProfilesList)
  })
_sym_db.RegisterMessage(CarProfilesList)

UsersList = _reflection.GeneratedProtocolMessageType('UsersList', (_message.Message,), {
  'DESCRIPTOR' : _USERSLIST,
  '__module__' : 'car_sales_pb2'
  # @@protoc_insertion_point(class_scope:car_sales.UsersList)
  })
_sym_db.RegisterMessage(UsersList)



_CAR_SALES = _descriptor.ServiceDescriptor(
  name='Car_sales',
  full_name='car_sales.Car_sales',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=627,
  serialized_end=947,
  methods=[
  _descriptor.MethodDescriptor(
    name='getAllPosts',
    full_name='car_sales.Car_sales.getAllPosts',
    index=0,
    containing_service=None,
    input_type=google_dot_protobuf_dot_empty__pb2._EMPTY,
    output_type=_CARPOSTSLIST,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='getAllUsers',
    full_name='car_sales.Car_sales.getAllUsers',
    index=1,
    containing_service=None,
    input_type=google_dot_protobuf_dot_empty__pb2._EMPTY,
    output_type=_USERSLIST,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='getAllCarProfile',
    full_name='car_sales.Car_sales.getAllCarProfile',
    index=2,
    containing_service=None,
    input_type=google_dot_protobuf_dot_empty__pb2._EMPTY,
    output_type=_CARPROFILESLIST,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='addPost',
    full_name='car_sales.Car_sales.addPost',
    index=3,
    containing_service=None,
    input_type=_CARPOST,
    output_type=google_dot_protobuf_dot_empty__pb2._EMPTY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='addOffer',
    full_name='car_sales.Car_sales.addOffer',
    index=4,
    containing_service=None,
    input_type=_CAROFFER,
    output_type=google_dot_protobuf_dot_empty__pb2._EMPTY,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_CAR_SALES)

DESCRIPTOR.services_by_name['Car_sales'] = _CAR_SALES

# @@protoc_insertion_point(module_scope)
