from django.contrib import admin

from .models import CarProfile, CarPost, CarOffer

admin.site.register(CarProfile)
admin.site.register(CarPost)
admin.site.register(CarOffer)
