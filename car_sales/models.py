from django.contrib.auth.models import User
from django.db import models


class CarProfile(models.Model):
    name = models.CharField(max_length=128)
    details = models.TextField(
        "Details", max_length=500, blank=True, help_text="")
    color = models.CharField("Color", max_length=15, help_text="Red", null=True)

    def __str__(self):
        return self.name


class CarPost(models.Model):
    car_profile = models.OneToOneField(CarProfile, on_delete=models.CASCADE,)
    created = models.DateField(auto_now=True)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"{self.user}\'s {self.car_profile.name}"


class CarOffer(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True)
    car_post = models.ForeignKey(
        CarPost, on_delete=models.CASCADE, verbose_name="Car Post")
    created = models.DateField(auto_now=True)
    suggestion = models.IntegerField("suggestion", help_text="1000 IRR")

    def __str__(self):
        return f"{self.user} -> {self.car_post}"
