# Generated by Django 3.0.5 on 2020-04-19 04:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('car_sales', '0002_auto_20200419_0357'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='cars_to_sales',
        ),
        migrations.AddField(
            model_name='carpost',
            name='user_profile',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='car_sales.UserProfile'),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='phone_num',
            field=models.IntegerField(blank=True, max_length=10, null=True),
        ),
    ]
