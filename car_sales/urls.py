from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path('auth/', obtain_auth_token, name="api_token_auth"),
    path('create_auth/', views.create_auth, name="create_auth"),
    path('all_posts/', views.get_all_post, name="all_posts"),
    path('add_post/', views.add_post, name="add_posts"),
    path('add_offer/', views.add_offer, name="add_offer")

]

